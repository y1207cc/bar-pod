import { CoreOptions } from '@chartwerk/core';
import { BarOptions, BarAdditionalOptions } from '../types';

import * as _ from 'lodash';

const BAR_SERIE_DEFAULTS = {
  renderBarLabels: false,
  stacked: false,
  barWidth: undefined,
  maxBarWidth: undefined,
  minBarWidth: undefined,
  maxAnnotationSize: undefined,
  minAnnotationSize: undefined,
  matching: false,
  opacityFormatter: undefined,
  annotations: [],
};

export class BarConfig extends CoreOptions<BarOptions> {

  constructor(options: BarOptions) {
    super(options, BAR_SERIE_DEFAULTS);
  }

  get barOptions(): BarAdditionalOptions {
    return {
      renderBarLabels: this._options.renderBarLabels,
      stacked: this._options.stacked,
      barWidth: this._options.barWidth,
      maxBarWidth: this._options.maxBarWidth,
      minBarWidth: this._options.minBarWidth,
      maxAnnotationSize: this._options.maxAnnotationSize,
      minAnnotationSize: this._options.minAnnotationSize,
      matching: this._options.matching,
      opacityFormatter: this._options.opacityFormatter,
      annotations: this._options.annotations,
    }
  }

  // event callbacks
  callbackContextMenu(data: any): void {
    if(_.has(this._options.eventsCallbacks, 'contextMenu')) {
      this._options.eventsCallbacks.contextMenu(data);
    }
  }

  get contextMenu(): (evt: any) => void {
    return this._options.eventsCallbacks.contextMenu;
  }
}
