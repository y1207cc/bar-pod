import { CoreSeries } from '@chartwerk/core';
import { BarSerie } from '../types';


const BAR_SERIE_DEFAULTS = {
  matchedKey: undefined,
  colorFormatter: undefined
};

export class BarSeries extends CoreSeries<BarSerie> {

  constructor(series: BarSerie[]) {
    super(series, BAR_SERIE_DEFAULTS);
  }
}
